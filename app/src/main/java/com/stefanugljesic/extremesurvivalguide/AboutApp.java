package com.stefanugljesic.extremesurvivalguide;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class AboutApp extends AppCompatActivity {
    DrawerLayout drawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);
        drawerLayout =findViewById(R.id.drawer_layout);
    }
    public void ClickMenu(View view)
    {
        MainActivity.openDrawer(drawerLayout);
    }
    public void ClickLog(View view)
    {
        MainActivity.closeDrawer(drawerLayout);
    }
    public void ClickHome(View view)
    {
        MainActivity.redirectActivity(this,MainActivity.class);
    }
    public void ClickDashboard(View view)
    {
        MainActivity.redirectActivity(this,Dashboard.class);
    }
    public void ClickAboutUs(View view)
    {
        recreate();
    }
    public void ClickLogout(View view){
        MainActivity.logout(this);
    }

    @Override
    protected  void onPause(){
        super.onPause();
        MainActivity.closeDrawer(drawerLayout);
    }
    public void Ferit(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ferit.hr"));
        startActivity(browserIntent);
    }
    public void Facebook(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com"));
        startActivity(browserIntent);
    }
    public void Instagram(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.instagram.com"));
        startActivity(browserIntent);
    }
    public void Discord(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.Discord.com"));
        startActivity(browserIntent);
    }
}