package com.stefanugljesic.extremesurvivalguide;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    String data1[], data2[];
    int images[];
    Context context;
    public MyAdapter(Context ct, String s1[],String s2[],int img[]){
        context=ct;
        data1=s1;
        data2=s2;
        images=img;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater =LayoutInflater.from(context);
        View view =inflater.inflate(R.layout.cell_dashboard,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.myText1.setText(data1[position]);
        holder.myText2.setText(data2[position]);
        holder.myImage.setImageResource(images[position]);

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (position){
                    case 0:
                        Intent intent =new Intent(context,Shelter.class);
                        context.startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 =new Intent(context,Water.class);
                        context.startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 =new Intent(context,Campfire.class);
                        context.startActivity(intent2);
                        break;
                    case 3:
                        Intent intent3 =new Intent(context,Food.class);
                        context.startActivity(intent3);
                        break;
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView myText1,myText2;
        ImageView myImage;
        LinearLayout mainLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            myText1=itemView.findViewById(R.id.naslov);
            myText2=itemView.findViewById(R.id.opis);
            myImage=itemView.findViewById(R.id.slika);
            mainLayout=itemView.findViewById(R.id.DashboardLayout);
        }
    }
}
