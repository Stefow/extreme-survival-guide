package com.stefanugljesic.extremesurvivalguide;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

public class Dashboard extends AppCompatActivity {

    private RecyclerView recycler;
    String s1[],s2[];
    int images[]={R.drawable.ic_shelter,R.drawable.ic_water,R.drawable.ic_campifre01,R.drawable.ic_cookingfood};
    DrawerLayout drawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        drawerLayout =findViewById(R.id.drawer_layout);
        recycler=findViewById(R.id.recyclerView);

        s1=getResources().getStringArray(R.array.dashboard_names);
        s2=getResources().getStringArray(R.array.description);
        MyAdapter myAdapter=new MyAdapter(this,s1,s2,images);
        recycler.setAdapter(myAdapter);
        recycler.setLayoutManager(new LinearLayoutManager(this));
    }
    public void ClickMenu(View view)
    {
        MainActivity.openDrawer(drawerLayout);
    }
    public void ClickLog(View view)
    {
        MainActivity.closeDrawer(drawerLayout);
    }
    public void ClickHome(View view)
    {
        MainActivity.redirectActivity(this,MainActivity.class);
    }
    public void ClickDashboard(View view)
    {
        recreate();
    }
    public void ClickAboutUs(View view)
    {
        MainActivity.redirectActivity(this,AboutApp.class);
    }
    public void ClickLogout(View view){
        MainActivity.logout(this);
    }

    @Override
    protected  void onPause(){
        super.onPause();
        MainActivity.closeDrawer(drawerLayout);
    }

}